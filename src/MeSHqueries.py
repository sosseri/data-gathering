import pandas as pd
import re
from df2gspread import gspread2df as g2d

def tidy_split(df, column, sep='|', keep=False):
    """
        Split the values of a column and expand so the new DataFrame has one split
        value per row. Filters rows where the column is missing.

        Params
        ------
        df : pandas.DataFrame
        dataframe with the column to split and expand
        column : str
        the column to split and expand
        sep : str
        the string used to split the column's values
        keep : bool
        whether to retain the presplit value as it's own row

        Returns
        -------
        pandas.DataFrame
        Returns a dataframe with the same columns as `df`.
        """
    indexes = list()
    new_values = list()
    df = df.dropna(subset=[column])
    for i, presplit in enumerate(df[column].astype(str)):
        values = presplit.split(sep)
        if keep and len(values) > 1:
            indexes.append(i)
            new_values.append(presplit)
        for value in values:
            indexes.append(i)
            new_values.append(value)
    new_df = df.iloc[indexes, :].copy()
    new_df[column] = new_values
    return new_df

def getTidyTaxonomy (spreadsheet):
    '''
    A function that returns the full MeSH taxonomy
    in tidy format from a spreadsheet were MeSH are grouped
    by unique ID.

    Mandatory arguments:
    i. spreadsheet - the ID of the google spreadsheet to load
    '''

    ## load the taxonomy from google
    mesh_taxonomy_df = g2d.download(spreadsheet, 'Full MeSH taxonomy', col_names = True)

    ## tidyfy
    mesh_taxonomy_df = mesh_taxonomy_df.set_index(['MeSH',\
                                'DUI'])['tree_number'].apply(lambda _ :\
                                    _.split('|')\
                                ).apply(pd.Series).stack().rename('tree_number').reset_index().drop('level_2', axis = 1)

    ## return
    return mesh_taxonomy_df

def filterMeSH (MeSH_df, fullTaxo = [False],\
        tree_column = 'Tree Number',\
            exclude_tree_col = 'Exclude',\
                exclude_mesh_col = False,
                tree_column_right = False):
    '''
    A function that prunes the MeSH tree according to
    a list of MeSH terms and returns the full list of
    corresponding terms.
    Mandatory arguments:
    i. MeSH_df - the dataframe of MeSH terms of interest

    Keyword arguments:
    i. fullTaxo - optionally, a dataframe with the whole MeSH taxonomy.
                    this needs to be passed if the whole MeSH taxonomy needs
                    to be pruned
    ii. tree_column - the name of the column storing the tree codes of
                        the terms to add
    iii. exclude_tree_col - the name of the column storing the tree codes of
                        the terms to remove
    iv. exclude_mesh_col - the name of the column storing the unique ID of
                        fi  the terms to remove
    v. tree_column_right - if pruning the whole MeSH taxonomy, the column
                            storing the tree codes of the terms

    Returns:
    i. mesh_list - the list of MeSH terms deemed pertinent
    '''

    ## add a column associated to the level in
    ## the taxonomic tree
    if not ('level' in MeSH_df.columns):
        MeSH_df['level'] = MeSH_df[tree_column].apply(lambda _ : _.count('.'))

    ## check if pruning the whole taxonomy
    if not (any(fullTaxo)):
        fullTaxo = MeSH_df
        tree_column_right = tree_column


    ## make a list of terms to exclude and include
    includeM = MeSH_df.set_index('Add').loc['TRUE'].reset_index()
    includeM = includeM.sort_values(by = 'level').set_index('level')
    try:
        excludeM = MeSH_df.set_index(exclude_tree_col).loc['TRUE'].reset_index()
        excludeM = excludeM.sort_values(by = 'level').set_index('level')
    except KeyError:
        excludeM = pd.DataFrame([], index = [10000])



    ## now, for the final perimeter to be consistent,
    ## the tree must per run from shallow to deep level.
    ## For each level, terms must be first added and then removed.
    ## Doing in this order is important so not to have
    ## terms that should be removed or viceversa
    
    mesh_list = pd.DataFrame()

    for l in MeSH_df.level.sort_values().unique():
        ## run over the tree levels
        if l in includeM.index:
            ## add terms
            this_rule = r'|'.join (includeM.loc[[l],tree_column].apply(lambda _ : re.escape(_)))
            this_rule = r'^(%s)' %this_rule

            mesh_list_this = fullTaxo[fullTaxo[tree_column_right].str.contains(this_rule)]

            ## if first level, initialise the list of possible terms
            if l == 0:
                mesh_list = mesh_list_this
            else:
                mesh_list = pd.concat([mesh_list, mesh_list_this], ignore_index= True, sort=False)

        ## remove terms
        if l in excludeM.index:

            this_rule = r'|'.join (excludeM.loc[[l],tree_column].apply(lambda _ : re.escape(_.strip())))
            this_rule = r'^(%s)' %this_rule


            mesh_list = mesh_list[~mesh_list[tree_column_right].str.contains(this_rule)].copy()

    ## if single terms (not whole tree chunks)
    ## should be excluded, do the following
    if exclude_mesh_col:
        mesh_list = mesh_list [~mesh_list.DUI.isin(MeSH_df.set_index('Exclude MESH').loc['TRUE',tree_column].values)]

    ## return the full list of MeSH terms
    return mesh_list

def getPerimeter (spreadsheet, tab, topic, fullTaxo = [False], return_regex = True):
    '''
    A function that downloads a googlesheet with a
    series of MeSH tree elements to be included/exluded
    and that returns list of unique terms (not tree branches) to add/exclude
    and a regex rule to explode MeSH trees.

    from the study. The sheet needs to have (at least) the following columns:
    ** Scope/Main MeSH Heading/Tree Number/Add/Exclude **

    Mandatory arguments :
    1. spreadsheet - the ID of the google sheet
    2. tab - the tab of the spreadhseet to be procesed

    Returns:
    1. current_subheadings_df_dui - a dataframe with the
                                    pertinent MeSH with unique ID to add/exclude
    2. current_perimeter - a regular expression to prune the taxonomy according
                                    to the selection
    '''

    ## download the data
    current_subheadings_df = g2d.download(spreadsheet, tab, col_names = True, row_names = True).loc[topic]

    current_subheadings_df = tidy_split(current_subheadings_df, 'Tree Number')
    ## initialise a column to
    ## store if wanting to add/remove stuff
    current_subheadings_df ['perimeter'] = pd.np.nan
    current_subheadings_df.loc [current_subheadings_df['Add'] == 'TRUE', 'perimeter'] = 'Add'
    current_subheadings_df.loc [current_subheadings_df['Exclude'] == 'TRUE', 'perimeter'] = 'Remove'

    ## remove '[' and ']' from the tree numbers
    current_subheadings_df['Tree Number'] = current_subheadings_df['Tree Number'].str.replace('[','').str.replace(']','').str.replace(' ','')

    ## make a difference from tree numbers and
    ## unique IDs of the MeSH terms
    current_subheadings_df_ = current_subheadings_df [~current_subheadings_df['Tree Number'].str.contains (r'^D\d\d\d\d\d\d*$')].reset_index()
    current_subheadings_df_dui = current_subheadings_df [current_subheadings_df['Tree Number'].str.contains (r'^D\d\d\d\d\d\d*$')].reset_index()

    ## create a column that stores
    ## the level depth in the taxonomic tree
    current_subheadings_df_['level'] = current_subheadings_df_['Tree Number'].apply(lambda _ : _.count('.'))
    
    ## filter MeSH and get a plain list
    current_perimeter = filterMeSH (current_subheadings_df_,
                                    fullTaxo = fullTaxo,
                                    tree_column = 'Tree Number', tree_column_right = 'tree_number')


    tmp = current_subheadings_df_dui.set_index('perimeter')
    try:
        current_perimeter = current_perimeter [~current_perimeter['DUI'].isin (tmp.loc[['Remove'], 'Tree Number'].values)]
    except KeyError:
            pass
    if any (fullTaxo):
        try:
            current_perimeter = pd.concat ([current_perimeter,\
                                       fullTaxo [fullTaxo['DUI'].isin (tmp.loc[['Add'], 'Tree Number'].values)]\
                                        ], sort = False)
        except KeyError:
            pass
    if not (return_regex):
        return current_perimeter
    ## create a regular expression of the terms to retain
    else:
        current_perimeter = pd.Series (  [r"^(%s)$" %'|'.join ([re.escape (x) for x in current_perimeter['tree_number']] )],\
                                                               index = pd.MultiIndex.from_tuples([(topic, 'Add')]) )



        return current_subheadings_df_dui, current_perimeter


def getAllMeSHfromPerimeter (mesh_taxonomy_df, current_subheadings_df_dui, current_perimeter, topic):
    '''
    A function that, given a full MeSH taxonomy,
    and a subset useful for a given scope, returns the
    full list of pertinent MeSH terms.

    Mandatory arguments:
    i. mesh_taxonomy_df - the full MeSH taxonomy
    ii. current_subheadings_df_dui - the list of MeSH terms for the perimeter of interest
                                    (it should feature a column DUI for the unique ID of the MeSH terms)
    iii. current perimeter - a regular expression for pruning the MeSH tree
    iv. topic - the name of the field of interest (must be in the current_subheadings_df_dui dataframe)

    Returns:
    i. theseMeSH - a dataframe with the list of MesH terms finally selected.
    '''

    ## check if there are 'loose'
    ## MeSH terms to remove
    try:
        dui_remove = current_subheadings_df_dui.set_index(['index', 'perimeter']).loc[(topic, 'Remove'), 'Tree Number'].values
    except KeyError:
        dui_remove = []
    ## check if there are 'loose'
    ## MeSH terms to add
    try:
        dui_add = current_subheadings_df_dui.set_index(['index', 'perimeter']).loc[(topic, 'Add'), 'Tree Number'].values
    except KeyError:
        dui_add = []

    ## return a list of MeSH terms from the full taxonomy
    ## by applying the regular expression
    theseMeSH = mesh_taxonomy_df[(mesh_taxonomy_df['tree_number'].str.contains (current_perimeter.loc[topic, 'Add']) | mesh_taxonomy_df.DUI.isin(dui_add)) & \
                (~mesh_taxonomy_df.DUI.isin(dui_remove))]

    return theseMeSH



def makeSubsetQueryPubMed (fulltaxo, baseline_df, baseline_name, extra_terms = [False]):

    '''
    A function that takes a dataframe of MeSH terms and
    builds a PubMed query.
    Mandatory arguments:
    i. fulltaxo - the full MeSH taxonomy
    ii. baseline_df - the dataframe with the MeSH perimeter of interest
    iii. baseline_name - the name of the perimeter of interest (e.g. Infectious diseases)

    Keyword arguments:
    i. extra_terms : a list of extra MeSH terms of interest

    Returns:
    i. query - a string that can be posted to Pubmed as a query
    '''

    ## set the index to contain the topic of interest
    ## e.g. Infectious Diseases
    if 'Scope' in  baseline_df.columns:
        work_bl = baseline_df .set_index('Scope')
    else:
        work_bl = baseline_df .copy()
        work_bl.index.names = ['Scope']

    ## rename columns to align names
    ## across vertical theems and whole biomed
    work_bl.rename(columns = {'Tree Number / ID' : 'Tree Number' ,'Exclude TREE' : 'Exclude'}, inplace = True)
    work_bl = tidy_split(work_bl, 'Tree Number')

    ## the highest elements of the taxonomy
    ## (e.g. 'Anatomy [A]') are not searched for in Pubmed
    ## we need to expand them into the elements at level 2
    ## e.g. Body Regions [A01], Musculoskeletal System [A02], etc
    ## here we do this explosion of the tree
    roots = fulltaxo[fulltaxo['tree_number'].apply(lambda _ : _.count('.')) == 0].copy()
    roots['root'] = roots['tree_number'].str[0]
    work_bl = pd.merge(work_bl.reset_index(), roots, left_on = 'Tree Number', right_on = 'root', how = 'left')

    work_bl.loc[~work_bl.root.isnull(), 'Tree Number'] = work_bl.loc[~work_bl.root.isnull(), 'tree_number']
    work_bl.loc[~work_bl.root.isnull(), 'Main MeSH Heading'] = work_bl.loc[~work_bl.root.isnull(), 'MeSH']
    work_bl.set_index('Scope', inplace = True)


    ## make a stacked dataframe
    ## with 'Add' / 'Remove' terms
    pubmed_Subset = work_bl [~work_bl['Tree Number'].str.contains(\
        r'^D\d\d\d\d\d\d*$')].set_index(['Main MeSH Heading'], append = True)[\
        ['Add', 'Exclude']].stack().replace('', pd.np.nan).dropna().reset_index('Main MeSH Heading')

    ## Make an 'OR' query with the terms to add
    add_terms = ' OR '.join (['''"%s"[MeSH Terms]'''%_ for _ in pubmed_Subset.loc[(baseline_name, 'Add'), 'Main MeSH Heading'].str.strip().unique()])

    ## if there are some extra terms
    ## add these
    if any (extra_terms):
        add_terms += ' OR ' + ' OR '.join (['''"%s"[MeSH Terms]'''%_ for _ in extra_terms])

    ## optionally remove terms
#     remove_terms =  ' OR '.join (['''"%s"[MeSH Terms]'''%_ for _ in pubmed_Subset.loc[(baseline_name, 'Exclude'), 'Main MeSH Heading'].str.strip().unique()])
#     if len (remove_terms):
#         query = '''((%s) NOT (%s))''' %(add_terms, remove_terms)
#     else:

    ## make the query
    query = '(%s)' %add_terms
    return (query)

def makePubmedQuery (fulltaxo, baseline_df, baseline_name, field_df, field_name, extra_terms = False):

    '''
    A function that combines with a logical AND
    2 PubMed queries: the first one defining the general perimeter and the second one
    defining the topic of interest.

    Mandatory arguments:
    i. fulltaxo - the full MeSH taxonomy
    ii. baseline_df - the dataframe with the full MeSH perimeter of interest
    iii. baseline_name - the name of the full perimeter of interest
    iv. field_df - the dataframe with the MeSH perimeter of interest
    v. field_name - the name of the perimeter of interest (e.g. Infectious diseases)

    Keyword arguments:
    i. extra_terms : a list of extra MeSH terms of interest

    Returns:
    i. query - a string that can be posted to Pubmed as a query
    '''
    q1 = makeSubsetQueryPubMed (fulltaxo, baseline_df, baseline_name, extra_terms = extra_terms)
    q2 = makeSubsetQueryPubMed (fulltaxo, field_df, field_name)

    query = '''(%s) AND (%s)''' %(q1, q2)

    return query
